#!/bin/sh

# This script will wipe the free space on all mounted local partitions. 
# I use this to clean virtual machines before shrinking a qcow2 image.
# 
# The related command for shrinking a qcow2 image is:
#   qemu-img convert -O qcow2 old_image.qcow2 new_image.qcow2
#
# Needs to be run as root.


if [ $USER != "root" ]
then
	echo "This script needs to be run as root."
	exit 1
fi

for partition in `mount | grep "(local" | cut -d' ' -f3`
do
	echo "Cleaning $partition"
	dd if=/dev/zero bs=1M of=$partition/zero.dat
	rm $partition/zero.dat
done

