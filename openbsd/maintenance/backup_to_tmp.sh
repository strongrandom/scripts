#!/bin/sh

# This script will backup all local partitions (except /tmp) to /tmp.
# I use this to backup remote systems. Note that, you will need a
# large enough /tmp to store the backups.
#
# Note: This script, as written, will always take full dumps. 
#       Modify if you want incremental backups.
#
# Needs to be run as root.


if [ $USER != "root" ]
then
        echo "This script needs to be run as root."
        exit 1
fi

DATE=`date +%Y%m%d`
PREFIX=`hostname`

for partition in `mount | grep "(local" | grep -v "/tmp" | cut -d' ' -f3`
do
	NAME=`echo $partition | sed 's/\//\./g'`
	FILENAME="$PREFIX.$DATE$NAME.gz"  
	# ^ Note: "." separator is already in $NAME due to the simplistic regex.

	echo "Backing up $partition to $FILENAME"

	/sbin/dump -0u -f - $partition | gzip -c > /tmp/$FILENAME
done

